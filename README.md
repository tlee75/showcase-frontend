# showcase-frontend

This project creates a CloudFront distribution with an Origin Access Identity pointing at an S3 bucket using Route53.
The Terraform state is stored in a remote S3 backend and uses DynamoDB for locking.

# Tech Stack

Terraform
GitLab CI
S3
Route53
CloudFront
AWS Certificate Manager
DynamoDB

# Requirements

ARN of a Valid ACM Certificate for the domains you'll be using with CloudFront.

Access ID/Key for an IAM user with programatic access to AWS and full access to S3, DynamoDB, CloudFront, Route53. 
Store key/secret details as pipeline environment variables:
`AWS_ACCESS_KEY_ID`  
`AWS_SECRET_ACCESS_KEY`


Create an S3 bucket and DynamoDB table for the backend. The bucket and table names should match the `bucket` and `dynamodb_table` 
keys in the s3 backend config. When creating the DynamoDB table, the partition key should be set to `LockID`

# Usage

`terraform apply -var-file="prod/us-east-1/prod1/terraform.tfvars"`

# Related Projects

[showcase-ui](https://gitlab.com/tlee75/showcase-ui) - The UI project that will be deployed to the S3 bucket
resource "aws_s3_bucket" "front_end_bucket" {
  bucket = "${var.inventory_code_tag}.${var.env_tag}.${var.region}.${var.domain_name}"
  acl    = "private"
  force_destroy = true
}

data "aws_iam_policy_document" "s3_iam_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.front_end_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "front_end_bucket_policy" {
  bucket = aws_s3_bucket.front_end_bucket.id
  policy = data.aws_iam_policy_document.s3_iam_policy.json
}

